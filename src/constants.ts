const constants = {
  outputType: [
    { value: "File", description: "Save to File" },
    { value: "Clipboard", description: "Copy to Clipboard" }
  ],
  styles: {
    strokeColor: "#00bc66",
    strokeWeight: 2,
    strokeOpacity: 0.7,
    fillColor: "#00bc66",
    fillOpacity: 0.3,
    streetViewControl: false
  },
  outputFormat: [
    { value: "JSON", description: "JSON" },
    { value: "CSV", description: "CSV" },
    { value: "Pair", description: "Pair" }
  ],
  outputMap: [
    { value: "MARKER", description: "Marker" },
    { value: "POLYLINE", description: "Polyline" },
    { value: "POLYGON", description: "Polygon" }
  ]
};

export default constants;
