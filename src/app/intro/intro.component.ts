import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { LoggingService } from "../services/logging.service";

@Component({
  selector: "app-intro",
  templateUrl: "./intro.component.html",
  styleUrls: ["./intro.component.scss"]
})
export class IntroComponent {
  constructor(private router: Router, private loggingService: LoggingService) {}
  ngOnInit() {
    this.loggingService.logDetails().subscribe();
  }
  redirect(route) {
    this.router.navigateByUrl(`/${route}`);
  }
}
