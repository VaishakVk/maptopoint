import { Component } from "@angular/core";
import { faUndo, faRedo, faSave } from "@fortawesome/free-solid-svg-icons";
import { NotificationService } from "../notification/notification.service";
import constants from "src/constants";
declare var google: any;

@Component({
  selector: "app-map",
  templateUrl: "./map.component.html",
  styleUrls: ["./map.component.scss"]
})
export class MapComponent {
  faUndo = faUndo;
  faRedo = faRedo;
  faSave = faSave;
  latitude: number = 0;
  longitude: number = 0;
  zoom: number = 2;
  currentPointer;
  polyLinePoints: Array<any> = [];
  polygonPoints: Array<any> = [];
  removedStack: Array<any> = [];
  outputType: Array<any> = constants.outputType;
  styles = constants.styles;
  outputFormat: Array<any> = constants.outputFormat;
  selectedOutputFormat: String = this.outputFormat[0].value;
  selectedOutputType: String = this.outputType[0].value;
  result: string = "";

  constructor(private notificationService: NotificationService) {}
  ngOnInit() {
    // this.setCurrentLocation();
  }

  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(position => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 15;
      });
    }
  }

  copyText() {
    let selBox = document.createElement("textarea");
    selBox.style.position = "fixed";
    selBox.style.left = "0";
    selBox.style.top = "0";
    selBox.style.opacity = "0";
    selBox.value = this.result;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand("copy");
    document.body.removeChild(selBox);
  }

  saveToFile() {
    let extension: string =
      this.selectedOutputFormat === "JSON"
        ? "json"
        : this.selectedOutputFormat === "CSV"
        ? "csv"
        : "txt";
    var pom = document.createElement("a");
    pom.setAttribute(
      "href",
      "data:text/plain;charset=utf-8," + encodeURIComponent(this.result)
    );

    let timestamp = new Date().getTime();
    pom.style.opacity = "0";
    pom.setAttribute("download", `Location_${timestamp}.${extension}`);
    pom.focus();
    pom.click();
  }
  pointerClicked(e) {
    console.log(e);
    this.addMarkerAndGeneratePolygon(e.coords);
  }
  addMarkerAndGeneratePolygon(location) {
    this.currentPointer = location;
    this.polyLinePoints.push(location);
    console.log(location);
  }

  popLastElement() {
    this.removedStack.push(this.polyLinePoints.pop());
    this.currentPointer = this.polyLinePoints[this.polyLinePoints.length - 1];
  }

  restoreLastElement() {
    let restoredPointer = this.removedStack.pop();
    console.log(restoredPointer);
    if (restoredPointer) this.addMarkerAndGeneratePolygon(restoredPointer);
  }
  convertToCSV() {
    this.result = "";
    this.result += "lng,lat\n";
    this.polyLinePoints.forEach(row => {
      this.result += `${row.lng},${row.lat}\n`;
    });
  }

  convertToTuple() {
    this.result = "";
    this.polyLinePoints.forEach(row => {
      this.result += `(${row.lng},${row.lat})\n`;
    });
  }
  transformToOutput() {
    if (this.selectedOutputFormat === "JSON")
      this.result = JSON.stringify(this.polyLinePoints);
    else if (this.selectedOutputFormat === "CSV") this.convertToCSV();
    else if (this.selectedOutputFormat === "Pair") this.convertToTuple();
  }

  performOutputAction() {
    if (this.selectedOutputType === "File") this.saveToFile();
    else if (this.selectedOutputType === "Clipboard") {
      this.copyText();
      this.notificationService.sendMessage("Copied!");
    }
  }
  generateCordinates() {
    if (this.polyLinePoints.length === 0)
      this.notificationService.sendMessage("No pointers are marked on the map");
    else {
      this.polyLinePoints.push(this.polyLinePoints[0]);
      this.polygonPoints = this.polyLinePoints;

      this.transformToOutput();
      this.performOutputAction();
    }
    // console.log(this.polygonPoints);
  }
}
