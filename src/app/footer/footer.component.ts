import { Component } from "@angular/core";
import { FooterService } from "./footer.service";
import { NotificationService } from "../notification/notification.service";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"]
})
export class FooterComponent {
  constructor(
    private footerService: FooterService,
    private notificationService: NotificationService
  ) {}
  contactForm = {
    email: "",
    name: "",
    issue: ""
  };

  onSubmit() {
    this.footerService
      .postFeeback(
        this.contactForm.name,
        this.contactForm.email,
        this.contactForm.issue
      )
      .subscribe(
        () => {
          this.notificationService.sendMessage(
            "Thank you. We will get back to you shortly!"
          );
        },
        () => {
          this.notificationService.sendMessage("Oops. Something went wrong!");
        }
      );
  }
}
