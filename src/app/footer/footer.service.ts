import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class FooterService {
  apiUrl: string = environment.apiUrl;

  constructor(private http: HttpClient) {}

  postFeeback(name, email, feedback) {
    let feedbackPost = { name, email, feedback };
    console.log(feedbackPost);
    return this.http.post(this.apiUrl + "feedback", feedbackPost);
  }
}
