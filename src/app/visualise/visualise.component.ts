import { Component, ViewChild, ElementRef } from "@angular/core";
import constants from "src/constants";
import { AgmMap } from "@agm/core";
declare var google: any;

@Component({
  selector: "app-visual",
  templateUrl: "./visualise.component.html",
  styleUrls: ["./visualise.component.scss"]
})
export class VisualiseComponent {
  outputFormat: Array<any> = constants.outputFormat;
  polygonLines: Array<any> = [];
  map_input;
  bounds;
  outputMap: Array<any> = constants.outputMap;
  selectedOutputFormat: string = this.outputFormat[0].value;
  selectedOutputMap: string = this.outputMap[0].value;
  styles = constants.styles;
  latitude: number;
  longitude: number;
  zoom: number = 10;
  @ViewChild("AgmMap", { read: ElementRef, static: false }) agmMap: AgmMap;
  mapInstance: any;

  submitData() {
    this.polygonLines = JSON.parse(this.map_input);
    this.latitude = this.polygonLines[0].lat;
    this.longitude = this.polygonLines[0].lng;
    this.mapReady(this.mapInstance);
  }

  mapReady(map) {
    this.mapInstance = map;
    // this.bounds;
    this.setBounds();
  }

  setBounds() {
    if (this.mapInstance) {
      this.bounds = new google.maps.LatLngBounds();
      this.polygonLines.forEach(location => {
        let lat = location.lat;
        let lng = location.lng;
        this.bounds.extend(new google.maps.LatLng(lat, lng));
      });
      this.mapInstance.fitBounds(this.bounds);
    }
  }
}
