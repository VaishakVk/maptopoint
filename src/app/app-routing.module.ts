import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { IntroComponent } from "./intro/intro.component";
import { MapComponent } from "./map/map.component";
import { VisualiseComponent } from "./visualise/visualise.component";

const routes: Routes = [
  { path: "", component: IntroComponent, pathMatch: "full" },
  { path: "generate", component: MapComponent },
  { path: "visualise", component: VisualiseComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
