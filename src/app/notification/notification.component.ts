import { Component } from "@angular/core";
import { Subject, Subscription } from "rxjs";
import { NotificationService } from "./notification.service";

@Component({
  selector: "app-notify",
  templateUrl: "./notification.component.html",
  styleUrls: ["./notification.component.scss"]
})
export class NotificationComponent {
  message;
  timeout;
  subscription: Subscription;
  constructor(private notificationService: NotificationService) {}
  alert(message) {
    this.message = message;
    if (this.timeout) clearTimeout(this.timeout);
    setTimeout(() => {
      this.message = null;
    }, 2000);
  }

  ngOnInit() {
    this.subscription = this.notificationService
      .getMessage()
      .subscribe(message => this.alert(message));
  }
}
