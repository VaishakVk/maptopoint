import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { AgmCoreModule } from "@agm/core";
import { MapComponent } from "./map/map.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { IntroComponent } from "./intro/intro.component";
import { HttpClientModule } from "@angular/common/http";
import { NotificationModule } from "./notification/notification.module";
import { FooterComponent } from "./footer/footer.component";
import { VisualiseComponent } from "./visualise/visualise.component";
import { environment } from "src/environments/environment";
@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    NavbarComponent,
    IntroComponent,
    FooterComponent,
    VisualiseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: environment.apiKey,
      libraries: ["places"]
    }),
    FormsModule,
    FontAwesomeModule,
    HttpClientModule,
    NotificationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
