import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class LoggingService {
  constructor(private http: HttpClient) {}
  apiUrl: string = environment.apiUrl;
  logDetails() {
    return this.http.post(this.apiUrl, {});
  }
}
